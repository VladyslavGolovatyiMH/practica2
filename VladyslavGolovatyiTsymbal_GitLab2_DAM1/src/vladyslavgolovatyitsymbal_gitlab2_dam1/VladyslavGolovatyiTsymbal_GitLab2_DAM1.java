/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vladyslavgolovatyitsymbal_gitlab2_dam1;

import java.util.Scanner;

/**
 *
 * @author DAM113
 */
public class VladyslavGolovatyiTsymbal_GitLab2_DAM1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);

        int opc = 0;
        while (opc != 5) {
            System.out.println("\n ***** MENÚ DE OPCIONES *****");
            System.out.println("1.- Cambio de unidades de horas a segundos");
            System.out.println("2.- Cambio de unidades de kilómetros a metros");
            System.out.println("3.- Conversión de km/h a m/s");
            System.out.println("4.- Submenu de cambio de unidades imperiales al sistema internacional");
            System.out.println("5.- Salir");
            System.out.println("Elija una opción: ");
            opc = sc.nextInt();
            switch (opc) {
                case 1:
                    int horas,
                     segundos;
                    System.out.println("Introduzca las horas: ");
                    horas = sc.nextInt();
                    segundos = horas * 3600;
                    System.out.println("Horas: " + horas + " = " + segundos + " segundos");
                    break;
                case 2:
                    int kilometros,
                     metros;
                    System.out.println("Introduzca los kilometros: ");
                    kilometros = sc.nextInt();
                    metros = kilometros * 1000;
                    System.out.println("Kilometros: " + kilometros + " = " + metros + " metros");
                    break;
                /*
                    Aqui añadimos la funcionalidad de conversion km/h a m/s.
                 */
                case 3:
                    double kmH,
                     mS;
                    System.out.println("Introduzca los kilometros hora: ");
                    kmH = sc.nextDouble();
                    mS = kmH / 3.6;
                    System.out.println("Kilometros: " + kmH + " = " + mS + " metros");
                    break;
                case 4:
                    int opc2 = 0;
                    while (opc2 != 4) {
                        System.out.println("ELIJA OPCIÓN: ");
                        System.out.println("1.- De Pies a Centimetros");
                        System.out.println("2.- De Pulgadas a Milímetros");
                        System.out.println("3.- De Millas a Kilómetros");
                        System.out.println("4.- Fin");
                        opc2 = sc.nextInt();
                        switch (opc2) {
                            case 1:
                                double pies,
                                 centimetros;
                                System.out.println("Introduzca los Pies: ");
                                pies = sc.nextDouble();
                                centimetros = pies * 30.48;
                                System.out.println("Resultado = " + centimetros);
                                break;
                            case 2:
                                double pulgadas,
                                 milimetros;
                                System.out.println("Introduzca las Pulgadas: ");
                                pulgadas = sc.nextDouble();
                                milimetros = pulgadas * 25.4;
                                System.out.println("Resultado = " + milimetros);
                                break;
                            case 3:
                                double millas,
                                 km;
                                System.out.println("Introduzca las Millas: ");
                                millas = sc.nextDouble();
                                km = millas * 91.44;
                                System.out.println("Resultado = " + km);
                                break;
                            default:
                                System.out.println("Opción incorrecta!");
                                break;
                        }
                        break;
                    }
                case 5:
                    System.out.println("Fin de la ejecución.");
                    break;
                default:
                    System.out.println("Opción incorrecta!");
                    break;
            }
        }
    }

}
